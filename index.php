<?php

require_once('vendor/autoload.php');

$thermostat = new \HeatMiserThermostatApi\Thermostat('192.168.1.100');

$client = new \HeatMiserThermostatApi\Client($thermostat);

//$client->override(21);

//$client->status(\HeatMiserThermostatApi\Requests\ChangeStatus::STATUS_HOME);
//$client->status(\HeatMiserThermostatApi\Requests\ChangeStatus::STATUS_AWAY);

//$client->override(19);

$client->disableOverride();
