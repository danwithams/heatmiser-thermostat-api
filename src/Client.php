<?php

namespace HeatMiserThermostatApi;

use GuzzleHttp\Client as GuzzleClient;
use HeatMiserThermostatApi\Requests\Override;
use HeatMiserThermostatApi\Requests\BaseRequest;
use HeatMiserThermostatApi\Requests\ChangeStatus;
use HeatMiserThermostatApi\Requests\DisableOverride;

class Client
{
    protected $thermostat;
    protected $client;

    public function __construct(Thermostat $thermostat)
    {
        $this->thermostat = $thermostat;
        $this->client = new GuzzleClient;
    }

    protected function send(BaseRequest $request)
    {
        $request->send();
    }

    /**
     * @return Thermostat
     */
    public function getThermostat()
    {
        return $this->thermostat;
    }

    /**
     * @param Thermostat $thermostat
     * @return Client
     */
    public function setThermostat($thermostat)
    {
        $this->thermostat = $thermostat;
        return $this;
    }

    /**
     * @return GuzzleClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param GuzzleClient $client
     * @return Client
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

	public function status($status)
    {
        $this->send(
            $this->resolve(ChangeStatus::class)
                ->setStatus($status)
        );
    }

    public function resolve($classname, ...$args)
    {
        return new $classname($this, ...$args);
    }

    public function override($temperature, $status = ChangeStatus::STATUS_HOME)
    {
        $this->status($status);

        $this->send(
            $this->resolve(Override::class)
                ->setTemperature($temperature)
                ->setStatus($status)
        );
    }

    public function disableOverride()
    {
        $this->send(
            $this->resolve(DisableOverride::class)
        );
    }
}

