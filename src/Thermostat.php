<?php

namespace HeatMiserThermostatApi;

class Thermostat
{
    protected $host;
    protected $user;
    protected $password;
    protected $ping;

    public function __construct($host)
    {
        $this->setHost($host);
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     * @return Thermostat
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Thermostat
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return Thermostat
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPing()
    {
        return $this->ping;
    }

    /**
     * @param mixed $ping
     * @return Thermostat
     */
    public function setPing($ping)
    {
        $this->ping = $ping;
        return $this;
    }
}
