<?php

namespace HeatMiserThermostatApi\Requests;

class DisableOverride extends BaseRequest
{
    public function send()
    {
        return $this->post(
            'basicset.htm',
            [
                'ovca' => 2, // enable override
                'hdca' => 2,
                'kylk' => 0, // key lock
            ]
        );
    }
}
