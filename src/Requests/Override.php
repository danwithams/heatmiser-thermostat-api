<?php

namespace HeatMiserThermostatApi\Requests;

class Override extends BaseRequest
{
    protected $temperature;
    protected $status;

    public function send()
    {
        $temperature = $this->getTemperature();
        $status = $this->getStatus();
        if ($this->isValidTemperature($temperature, $status)) {
            return $this->post(
                'basicset.htm',
                [
                    'ovca' => 1, // enable override
                    'hdca' => 2,
                    'ovdt' => 1,
                    'tvrd' => $temperature,
                    'kylk' => 0, // key lock
                ]
            );
        }

        throw new \InvalidArgumentException("Unable temperature $temperature requested.");
    }

    protected function isValidTemperature($temperature, $status)
    {
        return in_array($temperature, self::getTemperatures($status));
    }

    public static function getTemperatures($status = ChangeStatus::STATUS_HOME)
    {
        $low = $status === ChangeStatus::STATUS_HOME ? 5 : 7;
        $high = $status === ChangeStatus::STATUS_HOME ? 35 : 17;

        $temperatures = [];
        while ($low <= $high) {
            $temperatures[] = $low++;
        }

        return $temperatures;
    }

    /**
     * @return mixed
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * @param mixed $temperature
     * @return Override
     */
    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Override
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
}
