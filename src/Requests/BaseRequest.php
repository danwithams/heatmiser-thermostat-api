<?php

namespace HeatMiserThermostatApi\Requests;

use HeatMiserThermostatApi\Client;
use GuzzleHttp\Client as GuzzleClient;
use HeatMiserThermostatApi\Thermostat;

abstract class BaseRequest
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    abstract public function send();

    protected function post($endpoint, $params)
    {
        return $this->request(
            'POST',
            $endpoint,
            [
                'form_params' => $params,
            ]
        );
    }

    protected function request($method, $endpoint, $options)
    {
        return $this->client->getClient()->request(
            $method,
            "http://{$this->client->getThermostat()->getHost()}/$endpoint",
            $options
        );
    }
}
