<?php

namespace HeatMiserThermostatApi\Requests;

class ChangeStatus extends BaseRequest
{
    const STATUS_HOME = 2;
    const STATUS_AWAY = 1;

    protected $status;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return ChangeStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function send()
    {
        if ($this->isValidStatus($this->status)) {
            return $this->post('right.htm', [ 'actH' => $this->status ]);
        }

        throw new \InvalidArgumentException("Incorrect value $this->status for \$status argument.");
    }

    protected function isValidStatus($status)
    {
        return in_array($status, self::getStatuses());
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_HOME,
            self::STATUS_AWAY,
        ];
    }
}
